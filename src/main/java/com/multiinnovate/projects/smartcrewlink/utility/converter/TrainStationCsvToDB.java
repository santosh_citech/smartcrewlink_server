package com.multiinnovate.projects.smartcrewlink.utility.converter;

import com.multiinnovate.projects.smartcrewlink.services.dao.TrainStationVMDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author : ADMIN 2/28/2022
 * @created : 28 / 02 / 2022 - 1:56 PM
 **/

@Service("TrainTimeTableService")
public class TrainStationCsvToDB implements csvToDataBase {


    @Autowired(required = true)
    private TrainStationVMDao trainStationVMDao;

    public TrainStationCsvToDB() {

    }

    @Override
    public void processRecords(String[] columns) {


        int trainNo = Integer.parseInt(columns[0]);
        int stopNo = Integer.parseInt(columns[1]);
        String code = columns[2];
        Long DayOfJourney = Long.valueOf(columns[3]);

        int DayOfJourney0 = Integer.parseInt(columns[3]);

        String arrivalTime = columns[4];
        String departureTime = columns[5];
        Long distance = Long.valueOf(columns[6]);
        int distance0 = Integer.parseInt(columns[6]);
        String locoType = columns[7];

        trainStationVMDao.saveTrainStation(trainNo, stopNo, code, DayOfJourney, arrivalTime, departureTime, distance, locoType);

//        trainStationVMDao.createTrainStations(trainNo, stopNo, code, DayOfJourney0, arrivalTime, departureTime, distance0, locoType);

    }
}
