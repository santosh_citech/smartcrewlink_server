package com.multiinnovate.projects.smartcrewlink.utility.utils;

import com.multiinnovate.projects.smartcrewlink.models.Trains;
import com.multiinnovate.projects.smartcrewlink.repositories.StationRepository;
import com.multiinnovate.projects.smartcrewlink.repositories.TrainRepository;
import com.multiinnovate.projects.smartcrewlink.repositories.TrainStationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

/**
 * @author : ADMIN 3/1/2022
 * @created : 01 / 03 / 2022 - 4:29 AM
 **/

@SuppressWarnings("unused")
interface ITrainTimeTableUtilityTool {

    void calArrivalDay(Long dayOfJourney, Long arrivalTimeMinutes, Long departureTimeMinutes);

    void calDepartureDay(Long dayOfJourney, Long arrivalTimeMinutes, Long departureTimeMinutes);

    LocalDateTime getDepartureDateTime(String departureTime, Long dayOfJourney, Long arrivalTimeMinutes,
                                       Long departureTimeMinutes);

    LocalDateTime getArrivalDateTime(String arrivalTime, Long dayOfJourney);


    LocalTime parseLocalTime(String time);


    Long convertToMinutesWithDay(String time, Long day);


}

@SuppressWarnings("unused")
@Service("trainTimeTableUtilityTool")
@Transactional
public class TrainTimeTableUtilityTool implements Serializable, ITrainTimeTableUtilityTool {

    @PersistenceContext
    private final EntityManager entityManager;

    @Autowired
    TrainRepository trainRepository;

    @Autowired
    TrainStationRepository trainStationRepository;

    @Autowired
    StationRepository stationRepository;

    private static final Logger logger = LoggerFactory.getLogger(TrainTimeTableUtilityTool.class.getName());

    public TrainTimeTableUtilityTool(EntityManager entityManager) {
        this.entityManager = entityManager;

    }

    @Override
    public void calArrivalDay(Long dayOfJourney, Long arrivalTimeMinutes, Long departureTimeMinutes) {

    }

    @Override
    public void calDepartureDay(Long dayOfJourney, Long arrivalTimeMinutes, Long departureTimeMinutes) {

    }

    @Override
    public LocalDateTime getDepartureDateTime(String departureTime, Long dayOfJourney, Long arrivalTimeMinutes,
                                              Long departureTimeMinutes) {
        try {

            String str;
            String[] parts = departureTime.split(":");

            if (Integer.parseInt(parts[0]) <= 9 && Integer.parseInt(parts[0]) >= 0) {
                str = "2022-03-" + "0" + dayOfJourney + " " + "0" + departureTime.concat(":00");
            } else {
                str = "2022-03-" + "0" + dayOfJourney + " " + departureTime.concat(":00");
            }

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            return LocalDateTime.parse(str, formatter);
        } catch (DateTimeParseException dx) {
            System.out.println("Error getDepartureDateTime(): " + dx);

        }
        return null;
    }

    @Override
    public LocalDateTime getArrivalDateTime(String arrivalTime, Long dayOfJourney) {
        try {
            String str;
            String[] parts = arrivalTime.split(":");
            if (Integer.parseInt(parts[0]) <= 9 && Integer.parseInt(parts[0]) >= 0) {
                str = "2022-03-" + "0" + dayOfJourney + " " + "0" + arrivalTime.concat(":00");
            } else {
                str = "2022-03-" + "0" + dayOfJourney + " " + arrivalTime.concat(":00");
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            return LocalDateTime.parse(str, formatter);
        } catch (DateTimeParseException dx) {
            System.out.println("Error getArrivalDateTime(): " + dx);

        }
        return null;
    }


    @Override
    public LocalTime parseLocalTime(String time) {
        DateTimeFormatter parser = DateTimeFormatter.ofPattern("H:mm");
        return LocalTime.parse(time, parser);
    }


    @Override
    public Long convertToMinutesWithDay(String time, Long day) {

        if (!time.isEmpty()) {
            Long dayToMin = 24L * 60L;
            String[] timePartsStr = time.split(":");
            int[] timeParts = new int[timePartsStr.length];
            timeParts[0] = Integer.parseInt(timePartsStr[0]);
            timeParts[1] = Integer.parseInt(timePartsStr[1]);
            if ((timeParts[0] > 23 || timeParts[0] < 0) && (timeParts[1] > 59 || timeParts[1] < 0)) {
                throw new RuntimeException("Not valid dateTimeObj.time passed to convertDateTimeObj()");
            }
            return (day * dayToMin) + (timeParts[0] * 60L) + (timeParts[1]);
        }

        return null;
    }


}
