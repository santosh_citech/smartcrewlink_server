package com.multiinnovate.projects.smartcrewlink.utility.utils;

import org.springframework.util.StringUtils;

import java.io.File;
import java.io.Serializable;

/**
 * @author : ADMIN 2/28/2022
 * @created : 28 / 02 / 2022 - 10:33 PM
 **/
@SuppressWarnings("unused")
public class GetFileExtensionTool implements Serializable {

    public String getFileExtension(String filename, String choice) {

        switch (choice) {
            case "method-A":
                return StringUtils.getFilenameExtension(filename);
            case "method-B":
                if (filename == null) {
                    return "";
                }
                int i = filename.lastIndexOf('.');
                return i > 0 ? filename.substring(i + 1) : "";
            default:
                System.out.println("please provide your choices");

        }

        return "";
    }

    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }


    public static void main(String[] args) {


    }

}
