package com.multiinnovate.projects.smartcrewlink.utility.converter;

import org.springframework.stereotype.Service;

/**
 * @author : ADMIN 2/28/2022
 * @created : 28 / 02 / 2022 - 1:55 PM
 **/

@SuppressWarnings("unused")


public interface csvToDataBase {
    void processRecords(String[] columns);
}
