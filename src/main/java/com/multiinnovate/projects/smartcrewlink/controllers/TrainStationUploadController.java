package com.multiinnovate.projects.smartcrewlink.controllers;


import com.multiinnovate.projects.smartcrewlink.exceptions.CrewLinkException;
import com.multiinnovate.projects.smartcrewlink.utility.converter.csvToDataBase;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

/**
 * @author : ADMIN 2/28/2022
 * @created : 28 / 02 / 2022 - 2:33 PM
 **/

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@SuppressWarnings("unused")
public class TrainStationUploadController implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(TrainStationUploadController.class.getName());

    @Autowired(required = false)
    @Qualifier("TrainTimeTableService")
    private csvToDataBase trainTimeTableCsvToDatabase;


    @RequestMapping(value = "/UploadTrainStation", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public String UploadTrainStation(@RequestParam("file") MultipartFile file, HttpServletRequest httpservletrequest, HttpServletResponse httpServletResponse) throws ServletException, IOException, CrewLinkException {
        String[] columns;


        try {
            if (file.isEmpty()) {
                throw new RuntimeException("Please load a file");
            }
            int index = Objects.requireNonNull(file.getOriginalFilename()).lastIndexOf('.');
            if (index == -1) {
                return ""; // empty extension
            }
            if (index > 0) {
                String extension = "";
                extension = file.getOriginalFilename().substring(index + 1);
                if (!extension.equals("csv")) {
                    throw new CrewLinkException(" Please Upload file csv file !!!!");
                }
            }


            byte[] fileBytes = file.getBytes();
            int count = 0;
            int noRecords = 20;
            try {
                Reader reader = new InputStreamReader(file.getInputStream());
                CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();
                while ((columns = csvReader.readNext()) != null) {
                    if (count != noRecords) {
                        trainTimeTableCsvToDatabase.processRecords(columns);
                    } else {
                        break;
                    }
                    count++;
                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Exception in UploadTrainStation() " + Arrays.toString(e.getStackTrace()));
            }


        } catch (Exception ex) {
            throw new CrewLinkException("Error in TrainStationUpload !!!!", ex);
        }
        return "TrainTimeTable uploaded successfully!!!!";
    }

}
