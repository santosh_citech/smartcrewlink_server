package com.multiinnovate.projects.smartcrewlink.controllers;


import com.multiinnovate.projects.smartcrewlink.models.selection.SelectionModel;
import com.multiinnovate.projects.smartcrewlink.services.dao.TrainVMRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
public class TrainController {
    private static final Logger logger = LoggerFactory.getLogger(TrainController.class);

    @Autowired(required = false)
    TrainVMRepository trainVMRepository;


    @RequestMapping(value = "/listAllTrains", method = RequestMethod.GET)
    @ResponseBody
    public SelectionModel listAllTrains(
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false, defaultValue = "0") Long page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Long size

    ) {
        SelectionModel selectionModel = null;

        try {

            selectionModel = trainVMRepository.listALLTrains(sort, page, size);
        } catch (Exception e) {
            System.out.println("Error in  listAllTrains()" + e.getMessage());
            logger.error("Error in  listAllTrains()" + e.getMessage());
        }

        return selectionModel;
    }


}
