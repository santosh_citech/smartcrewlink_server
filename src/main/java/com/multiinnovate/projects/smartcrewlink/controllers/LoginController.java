package com.multiinnovate.projects.smartcrewlink.controllers;

import com.multiinnovate.projects.smartcrewlink.custom.models.UserVM;
import com.multiinnovate.projects.smartcrewlink.services.IUserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * @author : ADMIN 2/26/2022
 * @created : 26 / 02 / 2022 - 10:21 PM
 **/

@Controller
@SuppressWarnings("unused")
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired(required = false)
    IUserService iUserService;


    private void authenticate(HttpServletRequest request) {
        String upd = request.getHeader("authorization");
//        String pair = new String(Base64.decodeBase64(upd.substring(6)));
        String pair = new String(Base64.getDecoder().decode(upd.substring(6)));
        String userName = pair.split(":")[0];
        String password = pair.split(":")[1];
    }


    public String generateTokenKey(String username, String password, String email, String rolename) {

        String secret = "asdfSFS34wfsdfsdfSDSD32dfsddDDerQSNCK34SOWEK5354fdgdf4";

        Key hmacKey = new SecretKeySpec(Base64.getDecoder().decode(secret),
                SignatureAlgorithm.HS256.getJcaName());

        Instant now = Instant.now();
        String jwtToken = Jwts.builder()
                .claim("username", username)
                .claim("password", password)
                .claim("email", email)
                .claim("role ", rolename)
                .setSubject("jane")
                .setId(UUID.randomUUID().toString())
                .setIssuedAt(Date.from(now))
                .setExpiration(Date.from(now.plus(5L, ChronoUnit.MINUTES)))
                .signWith(hmacKey)
                .compact();

        return jwtToken;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> UserLogin(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        String username = null;
        String password = null;
        final String authorization = httpServletRequest.getHeader("Authorization");

        if (authorization != null && authorization.toLowerCase().startsWith("basic")) {
            String base64Credentials = authorization.substring("Basic".length()).trim();
            byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
            String credentials = new String(credDecoded, StandardCharsets.UTF_8);
            final String[] values = credentials.split(":", 2);
            username = values[0];
            password = values[1];
        }

        Map<String, String> userOBJ = new HashMap<>();

        UserVM userVM = iUserService.findByUsernameAndPassword(username, password);

        String username0 = userVM.getUsername();
        String password0 = userVM.getPassword();
        String email = userVM.getEmail();
        String roleName = userVM.getRolename();
        String token = generateTokenKey(username0, password0, email, roleName);
        userOBJ.put("username", username0);
        userOBJ.put("password", password0);
        userOBJ.put("email", email);
        userOBJ.put("roleName", roleName);
        userOBJ.put("token", token);

        String[] cookies = new String[100];
        cookies[0] = "username:" + username0;
        cookies[1] = "password:" + password0;
        cookies[2] = "email:" + email;
        cookies[3] = "roleName:" + roleName;
        cookies[4] = "token:" + token;


        Cookie cookie = new Cookie("username", username0);

        //add cookie to response
        httpServletResponse.addCookie(cookie);
        cookie.setMaxAge(7 * 24 * 60); // expires in 7 days


        return userOBJ;
    }


}
