package com.multiinnovate.projects.smartcrewlink.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

/**
 * @author : ADMIN 2/28/2022
 * @created : 28 / 02 / 2022 - 12:57 PM
 **/

//@Controller
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TrainStationController implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(TrainStationController.class);


    @RequestMapping(value = "/listAllStation", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void listAllStation(
            @RequestParam(value = "sort") String sort,
            @RequestParam(value = "limit") Long limit,
            @RequestParam(value = "page") Long page) {
        System.out.println("DSADAS");
    }


}
