package com.multiinnovate.projects.smartcrewlink.services.daoImpl;

import com.multiinnovate.projects.smartcrewlink.custom.models.TrainVM;
import com.multiinnovate.projects.smartcrewlink.models.selection.PaginationDetails;
import com.multiinnovate.projects.smartcrewlink.models.selection.SelectionModel;
import com.multiinnovate.projects.smartcrewlink.services.dao.TrainVMRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

@Service
public class TrainVMImplRepository implements TrainVMRepository {

    @PersistenceContext
    private final EntityManager entityManager;
    private static final Logger log = LoggerFactory.getLogger(TrainVMImplRepository.class);

    public TrainVMImplRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }









    @Override
    public SelectionModel listALLTrains(String sort, Long page, Long size) {
        Long totalElements = 0L;
        if (sort != null && sort.isEmpty()) {
            sort = null;
        }

        String to01 = "SELECT COUNT(distinct tt.id) ";

        String From01 = "FROM train AS tt LEFT JOIN station AS ss ON(tt.source_id=ss.id) " +
                "LEFT JOIN station AS ds ON(tt.destination_id = ds.id) " +
                "LEFT JOIN train_type AS ttp ON(tt.train_type_id = ttp.id)";

        String query0 = to01 + From01;
        try {
            Query query1f = entityManager.createNativeQuery(query0);

            totalElements = ((BigInteger) query1f.getSingleResult()).longValue();



        } catch (Exception e) {
            log.error("SQL ERROR IN Query01 :" + e);
        }

        String to = "SELECT " +
                "tt.train_no as trainNo," +
                "tt.train_name as trainName, " +
                "ss.code AS fromStation," +
                "ss.name AS fromStationName," +
                "tt.start_day as startDay ," +
                "ds.code AS destination, " +
                "ds.name AS destinationName, " +
                "ttp.name as trainType ";

        String From = "FROM train AS tt " +
                "LEFT JOIN station AS ss ON(tt.source_id=ss.id) " +
                "LEFT JOIN station AS ds ON(tt.destination_id = ds.id) " +
                "LEFT JOIN train_type AS ttp ON(tt.train_type_id = ttp.id) " +
                "ORDER BY :sort " +
                "LIMIT :size " +
                "OFFSET :page ";

        String query = to + From;
        try {
            Query query1 = entityManager.createNativeQuery(query);
            query1.setParameter("sort", sort);
            query1.setParameter("page", page);
            query1.setParameter("size", size);

            List<?> list = query1.getResultList();

            Long startIndex = (page * size);
            Long totalPages = (totalElements / size);

            String baseItemRestUri = "/listAllTrains";
            return new SelectionModel(TrainVM.class, list,
                    new PaginationDetails(totalElements,
                            startIndex,
                            page,
                            totalPages,
                            baseItemRestUri,
                            totalElements,
                            size,
                            page,
                            null, null, true, true),
                    null);
        } catch (Exception e) {
            log.error("SQL ERROR :" + e);
        }
        return null;
    }

}
