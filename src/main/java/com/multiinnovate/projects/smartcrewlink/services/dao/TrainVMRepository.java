package com.multiinnovate.projects.smartcrewlink.services.dao;



import com.multiinnovate.projects.smartcrewlink.models.Trains;
import com.multiinnovate.projects.smartcrewlink.models.selection.SelectionModel;

import java.util.List;
import java.util.Map;

public interface TrainVMRepository {



    SelectionModel listALLTrains(String sort, Long page, Long size);
}
