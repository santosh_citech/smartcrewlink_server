package com.multiinnovate.projects.smartcrewlink.services.servicesImpl;

import com.multiinnovate.projects.smartcrewlink.models.selection.SelectionModel;

/**
 * @author : ADMIN 2/28/2022
 * @created : 28 / 02 / 2022 - 1:25 PM
 **/
public interface ITrainStationService {

    SelectionModel listAllStation();
}
