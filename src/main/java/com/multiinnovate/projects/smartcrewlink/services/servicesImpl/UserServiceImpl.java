package com.multiinnovate.projects.smartcrewlink.services.servicesImpl;

import com.multiinnovate.projects.smartcrewlink.custom.models.UserVM;
import com.multiinnovate.projects.smartcrewlink.models.User;
import com.multiinnovate.projects.smartcrewlink.repositories.UserRepository;
import com.multiinnovate.projects.smartcrewlink.services.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * @author : ADMIN 2/26/2022
 * @created : 26 / 02 / 2022 - 9:54 PM
 **/

@Service
@SuppressWarnings("unused")

public class UserServiceImpl implements IUserService {

    @Autowired(required = false)
    UserRepository userRepository;

    @PersistenceContext
    private final EntityManager entityManager;
    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    public UserServiceImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<User> findAll() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public User findOne(Long id) {
//        return userRepository.findOne(id);

        return null;
    }

    @Override
    @Transactional
    public void delete(Long id) {
//        userRepository.delete(id);
    }

    @Override
    public User findByUsernameAndIsActive(String username, String password) {
        return userRepository.findByUsernameAndIsActive(username, password);
    }


    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings("SpellCheckingInspection")
    public UserVM findByUsernameAndPassword(String username, String password) {
        UserVM list = null;
        String q = "SELECT u.username,u.password,r.rolename,ue.name FROM user AS u " +
                "LEFT JOIN role AS r ON (r.id = u.role_id) " +
                "LEFT JOIN user_email AS ue ON(ue.id=u.email_id) " +
                "WHERE u.username = :username AND u.password = :password";

        try {
            Query query2f = entityManager.createNativeQuery(q);
            query2f.setParameter("username", username);
            query2f.setParameter("password", password);
            List<Object[]> list1 = query2f.getResultList();
            if (!list1.isEmpty()) {
                for (Object[] result : list1) {
                    String username0 = (String) result[0];
                    String password0 = (String) result[1];
                    String role = (String) result[2];
                    String email = (String) result[3];

                    list = new UserVM(list1, username0, password0, role, email);
                }
            }

        } catch (Exception ex) {
            System.out.println("Error : findByUsernameAndPassword()" + ex.getStackTrace().toString());
            log.error("Error :" + ex.getMessage().toString());
        }

        return list;
    }
}
