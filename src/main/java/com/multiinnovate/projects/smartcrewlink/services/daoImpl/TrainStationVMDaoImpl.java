package com.multiinnovate.projects.smartcrewlink.services.daoImpl;


import com.multiinnovate.projects.smartcrewlink.models.selection.ProcessResult;
import com.multiinnovate.projects.smartcrewlink.repositories.StationRepository;
import com.multiinnovate.projects.smartcrewlink.repositories.TrainRepository;
import com.multiinnovate.projects.smartcrewlink.repositories.TrainStationRepository;
import com.multiinnovate.projects.smartcrewlink.services.dao.TrainStationVMDao;
import com.multiinnovate.projects.smartcrewlink.utility.library.TimeCalculator;
import com.multiinnovate.projects.smartcrewlink.utility.utils.TrainTimeTableUtilityTool;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.math.BigInteger;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TrainStationVMDaoImpl implements TrainStationVMDao {

    @PersistenceContext
    private final EntityManager entityManager;
    private static final Logger logger = LoggerFactory.getLogger(TrainStationVMDaoImpl.class);
    private final TimeCalculator timeCalculator;


    @Qualifier("trainTimeTableUtilityTool")
    @Autowired(required = false)
    TrainTimeTableUtilityTool trainTimeTableUtilityTool;

    @Autowired(required = false)
    StationRepository stationRepository;

    @Autowired(required = false)
    TrainRepository trainRepository;


    @Autowired
    TrainStationRepository trainStationRepository;


    TrainStationVMDaoImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
        timeCalculator = new TimeCalculator();

    }

    @Transactional
    public void updateSTrainStation(String arrivalTime, String departureTime, Long dayOfJourney, Long distance, String code, int stopNumber, int startDay, Long ts_id) {

        Long stationId = stationRepository.findStation(code).longValue();
        Long calcDay = (startDay + dayOfJourney - 1) % 7;
        String q1 = "UPDATE train_station SET " +
                "arrival = :arrivalTime," +
                "departure = :departureTime," +
                "day_of_journey= :dayOfJourney," +
                "distance = :distance," +
                "station_id = :stationId," +
                "stop_no= :stopNumber," +
                "train_id = :t_id," +
                "journey_duration = 0," +
                "day =:calcDay " +
                "WHERE id = :ts_id";

        Query query = entityManager.createNativeQuery(q1);
        query.setParameter("arrivalTime", arrivalTime);
        query.setParameter("departureTime", departureTime);
        query.setParameter("dayOfJourney", dayOfJourney);
        query.setParameter("distance", distance);
        query.setParameter("stationId", stationId);
        query.setParameter("stopNumber", stopNumber);
        query.setParameter("calcDay", calcDay);
        query.setParameter("ts_id", ts_id);

        query.executeUpdate();
    }

    public long getTimeDifference(String time01, String time02, String choice) throws ParseException {
        long result = 0;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");


        LocalTime dd1 = LocalTime.parse(time01, formatter);
        LocalTime dd2 = LocalTime.parse(time02, formatter);

        switch (choice) {
            case "milli":
                result = Math.abs(java.time.Duration.between(dd1, dd2).toMillis());
                break;
            case "second":
                result = Math.abs(java.time.Duration.between(dd1, dd2).getSeconds());
                break;

            case "minutes":
                result = Math.abs(java.time.Duration.between(dd1, dd2).toMinutes());
                break;

        }

        return result;

    }

    private void calculateJourneyDuration(Integer trainNo, Integer stopNo, String code, Long dayOfJourney, String arrivalTime, String departureTime, Long distance) {

        java.sql.Time ts1Dep = null;
        int ts1Day;
        java.sql.Time ts1Arr = null;
        Long ts1JD;
        int ts1ADay;
        int ts1DDay;

        try {
            List<Object[]> trainResult = trainRepository.findTrainNoAndStartDay(trainNo);


            for (Object[] result : trainResult) {
                BigInteger TrainId = (BigInteger) result[0];
                int startDay = (int) result[1];

                BigInteger ts_id = trainStationRepository.findTrainStationId(TrainId, stopNo);
                System.out.println("id = " + ts_id);

                if (ts_id != null) {
                    List<Object[]> trainStationList = trainStationRepository.findByJourneyDurationAndDepartureTime(TrainId.longValue(), (stopNo - 1));
                    for (Object[] result0 : trainStationList) {
                        ts1Dep = (java.sql.Time) result0[0];
                        ts1Day = (int) result0[1];
                        ts1Arr = (java.sql.Time) result0[2];
                        ts1JD = (Long) result0[3];
                        if (ts1Dep == null) {
                            updateSTrainStation(arrivalTime, departureTime, dayOfJourney, distance, code, stopNo, startDay, TrainId.longValue());

                        } else {

                            ts1ADay = ts1Day;
                            ts1DDay = ts1Day;
                            if (getTimeDifference(ts1Dep.toString(), ts1Arr.toString(), "minutes") < 0) {
                                ts1Day = (ts1Day + 1) % 7;
                                ts1DDay = ts1Day;
                            }

                            long ts2Day = (startDay + dayOfJourney - 1) % 7;

                        }

                    }
                } else {

                    // insert train sstation with journey duration

                }
            }


        } catch (Exception ex) {
            System.out.println("Error in calculateJourneyDuration(): " + ex.getMessage());
            logger.error("Error in calculateJourneyDuration():" + ex.getMessage());

        }

    }


    @Override
    @Transactional
    public void saveTrainStation(Integer trainNo, Integer stopNo, String code, Long DayOfJourney, String arrivalTime,
                                 String departureTime, Long distance, String locoType) {

        try {


            BigInteger station = stationRepository.findStation(code);

            if (station == null) {
                this.stationRepository.saveStationData("santosh", 30L, 30L, 1L, 30L, 30L);
            } else {
                System.out.println("Station already Exists!!");
            }


            Long codeId = stationRepository.findStation(code).longValue();
            List<BigInteger> trainId = trainRepository.findByTrainId(trainNo);
            BigInteger trainId0 = trainId.get(0);
            LocalTime arrivalLocalTime = trainTimeTableUtilityTool.parseLocalTime(arrivalTime);
            LocalTime departureLocalTime = trainTimeTableUtilityTool.parseLocalTime(departureTime);
            Long arrivalDay = DayOfJourney - 1;
            Long departureDay = arrivalDay;

            Long arrivalInMin = trainTimeTableUtilityTool.convertToMinutesWithDay(arrivalTime, arrivalDay);
            Long departureInMin = trainTimeTableUtilityTool.convertToMinutesWithDay(departureTime, departureDay);

            LocalDateTime arrivalDateTime = trainTimeTableUtilityTool.getArrivalDateTime(arrivalTime, DayOfJourney);
            LocalDateTime departureDateTime = trainTimeTableUtilityTool.getDepartureDateTime(departureTime, DayOfJourney, 0L, 0L);


            calculateJourneyDuration(trainNo, stopNo, code, DayOfJourney, arrivalTime,
                    departureTime, distance);


        } catch (HibernateException exe) {
            System.out.println("Error in insertInDB() " + exe.getMessage());
            logger.error("Error in insertInDB() " + exe.getMessage());
        }

    }


    @Override
    @Transactional
    public List<Map<Object, Object>> list(String sort, Long limit, Long page, Long size) {

        String FROM = "	FROM train_station as ts"
                + "	LEFT JOIN station AS fs ON (ts.station_id = fs.id)"
                + "	LEFT JOIN train AS tt ON (tt.id = ts.train_id)";
        String query1 = "SELECT COUNT(ts.id)"
                + FROM;
        try {
            Query query1f = entityManager.createNativeQuery(query1);
            Long totalElements = ((java.math.BigInteger) query1f.getSingleResult()).longValue();
            Long startIndex = (page * size);
            Long totalPages = totalElements / size;
            Long currentPage = page;

            if (totalElements > 0) {
                if (sort != null && sort.isEmpty()) {
                    sort = null;
                }

                String FROM0 = " FROM train_station as ts"
                        + "	LEFT JOIN station AS fs ON (ts.station_id = fs.id)"
                        + "	LEFT JOIN train AS tt ON (tt.id = ts.train_id)"
                        + "	INNER JOIN train_type AS tty ON (tty.id = tt.train_type_id)";
                String query2 = "SELECT ts.station_code as code," +
                        " tt.train_no as trainNo," +
                        " ts.arrival_time as arrival," +
                        " ts.departure_time as departure," +
                        " tt.train_name as trainName," +
                        " tty.name as TrainType"

                        + FROM0 + " LIMIT :start, :offset";
                Query query2f = entityManager.createNativeQuery(query2);
                query2f.setParameter("start", page * size);
                query2f.setParameter("offset", size);

                List<Object[]> resultList = (List<Object[]>) query2f.getResultList();
                List<Map<Object, Object>> ret = new ArrayList<Map<Object, Object>>();
                for (Object[] result : resultList) {
                    Map<Object, Object> row = new HashMap<>();
                    row.put("code", result[0]);
                    row.put("trainNo", result[1]);
                    row.put("arrivalTime", result[2]);
                    row.put("departureTime", result[3]);
                    row.put("trainName", result[4]);
                    row.put("trainType", result[5]);
                    ret.add(row);
                }
                return ret;

            }

        } catch (Exception e) {
            logger.error("SQL Error" + e.getMessage());
        }

        return null;
    }

    @Override
    @Transactional(readOnly = false)
    public ProcessResult createTrainStations(int trainNo, int stopNumber, String stationCode, int dayOfJourney, String arrivalTime, String departureTime, long distance, String locotype) {
        ProcessResult resultO = null;

        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("createTrainStation");
        storedProcedure.registerStoredProcedureParameter("trainNo", Integer.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("stopNumber", Integer.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("stationCode", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("dayOfJourney", Integer.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("arrivalTime", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("departureTime", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("distance", Long.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("result", Boolean.class, ParameterMode.OUT);
        storedProcedure.registerStoredProcedureParameter("errorMessage", String.class, ParameterMode.OUT);

        storedProcedure.setParameter("trainNo", trainNo);
        storedProcedure.setParameter("stopNumber", stopNumber);
        storedProcedure.setParameter("stationCode", stationCode);
        storedProcedure.setParameter("dayOfJourney", dayOfJourney);
        storedProcedure.setParameter("arrivalTime", arrivalTime);
        storedProcedure.setParameter("departureTime", departureTime);
        storedProcedure.setParameter("distance", distance);

        storedProcedure.execute();

        Boolean result = (Boolean) storedProcedure.getOutputParameterValue("result");
        String errorMessage = storedProcedure.getOutputParameterValue("errorMessage").toString();
        resultO = new ProcessResult(result, errorMessage);
        return resultO;
    }


}