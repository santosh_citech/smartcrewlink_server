package com.multiinnovate.projects.smartcrewlink.services;

import com.multiinnovate.projects.smartcrewlink.custom.models.UserVM;
import com.multiinnovate.projects.smartcrewlink.models.User;

import java.util.List;

/**
 * @author : ADMIN 2/26/2022
 * @created : 26 / 02 / 2022 - 9:51 PM
 **/

@SuppressWarnings("unused")
public interface IUserService {

    List<User> findAll();

    void save(User user);

    User findOne(Long id);

    void delete(Long id);

    User findByUsernameAndIsActive(String username, String password);

    UserVM findByUsernameAndPassword(String username, String password);
}
