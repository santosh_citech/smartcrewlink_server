package com.multiinnovate.projects.smartcrewlink.services.dao;

import com.multiinnovate.projects.smartcrewlink.models.selection.ProcessResult;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


public interface TrainStationVMDao {

    void saveTrainStation(Integer trainNo, Integer stopNo, String code, Long DayOfJourney, String arrivalTime, String departureTime, Long distance, String locoType);

    List<Map<Object, Object>> list(String sort, Long limit, Long page, Long size);

    ProcessResult createTrainStations(int trainNo, int stopNumber, String stationCode, int dayOfJourney, String arrivalTime, String departureTime, long distance, String locotype);


}
