package com.multiinnovate.projects.smartcrewlink;

import com.multiinnovate.projects.smartcrewlink.components.TimeCalculatorComponent;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;


@ComponentScan(basePackages = {
        "com.multiinnovate.projects.smartcrewlink",
        "com.multiinnovate.projects.smartcrewlink.utility.converter",
        "com.multiinnovate.projects.smartcrewlink.utility.library",
        "com.multiinnovate.projects.smartcrewlink.utility.utils",
        "com.multiinnovate.projects.smartcrewlink.services.dao",
        "com.multiinnovate.projects.smartcrewlink.services.daoImpl",
        "com.multiinnovate.projects.smartcrewlink.controllers",
        "com.multiinnovate.projects.smartcrewlink.custom.controllers",
        "com.multiinnovate.projects.smartcrewlink.custom.model",
        "com.multiinnovate.projects.smartcrewlink.custom.repository",

        "com.multiinnovate.projects.smartcrewlink.exception",
        "com.multiinnovate.projects.smartcrewlink.models",
        "com.multiinnovate.projects.smartcrewlink.repositories",
        "com.multiinnovate.projects.smartcrewlink.services",
        "com.multiinnovate.projects.smartcrewlink.components"

})
@Configuration
@EnableAutoConfiguration
@SpringBootApplication
public class SmartcrewlinkApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {

        SpringApplication.run(SmartcrewlinkApplication.class, args);

    }


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SmartcrewlinkApplication.class);
    }

    @Bean
    CommandLineRunner init() throws ParseException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("com.multiinnovate.projects.smartcrewlink.components");
        context.refresh();
        TimeCalculatorComponent ms = context.getBean(TimeCalculatorComponent.class);
        ms.getTimeDifference();
        context.close();
        System.out.println("command runner running using smartlink bean class !");
        return null;
    }


}
