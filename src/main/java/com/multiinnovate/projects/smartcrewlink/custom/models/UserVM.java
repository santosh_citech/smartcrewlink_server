package com.multiinnovate.projects.smartcrewlink.custom.models;

import java.util.List;

/**
 * @author : ADMIN 2/26/2022
 * @created : 26 / 02 / 2022 - 5:42 PM
 **/
public class UserVM {

    private String username;
    private String password;
    private String rolename;
    private String email;
    private List<Object[]> data;
    private String errorMessage;

    public UserVM(List data, String username, String password, String rolename, String email) {
        super();
        this.data = data;
        this.username = username;
        this.password = password;
        this.rolename = rolename;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Object[]> getData() {
        return data;
    }

    public void setData(List<Object[]> data) {
        this.data = data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
