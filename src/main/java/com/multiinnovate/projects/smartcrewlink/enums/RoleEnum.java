package com.multiinnovate.projects.smartcrewlink.enums;


public enum RoleEnum {
    ADMIN,USER,SUPER,SUPERUSER,EMPLOYEE,MANAGER,PROGRAMMER,CHIEF,MODERATOR
}
