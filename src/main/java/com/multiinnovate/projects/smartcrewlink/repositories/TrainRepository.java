package com.multiinnovate.projects.smartcrewlink.repositories;

import com.multiinnovate.projects.smartcrewlink.models.Trains;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * @author : ADMIN 2/26/2022
 * @created : 26 / 02 / 2022 - 9:43 PM
 **/
@SuppressWarnings("unused")
@Repository
public interface TrainRepository extends JpaRepository<Trains, Long> {

    @Query(value = "SELECT t.id,t.start_day FROM train as t WHERE t.train_no =:trainNo", nativeQuery = true)
    List<Object[]> findTrainNoAndStartDay(int trainNo);

    @Query(value = "SELECT COUNT(t.id) FROM train as t WHERE t.train_no =:trainNo", nativeQuery = true)
    BigInteger getRecordsCount(@Param("trainNo") Integer trainNo);

    @Query(value = "SELECT DISTINCT(t.id) FROM train AS t WHERE t.train_no = :trainNo", nativeQuery = true)
    List<BigInteger> findByTrainId(@Param("trainNo") Integer trainNo);


}
