package com.multiinnovate.projects.smartcrewlink.repositories;

import com.multiinnovate.projects.smartcrewlink.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * @author : ADMIN 2/26/2022
 * @created : 26 / 02 / 2022 - 9:43 PM
 **/
@SuppressWarnings("unused")
public interface UserRepository extends CrudRepository<User, Long> {

    @Query("SELECT u FROM User as u where u.username = :username and u.password = :password")
    User findByUsernameAndIsActive(@Param("username") String username, @Param("password") String password);

//    User findOne(Long id);
//    Boolean delete(Long id);


}
