package com.multiinnovate.projects.smartcrewlink.repositories;

import com.multiinnovate.projects.smartcrewlink.models.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;

/**
 * @author : ADMIN 3/1/2022
 * @created : 01 / 03 / 2022 - 7:31 PM
 **/
@Repository
@SuppressWarnings("unused")
public interface StationRepository extends JpaRepository<Station, Long> {
    @Query(value = "SELECT ss.id FROM station AS ss WHERE ss.code LIKE :code", nativeQuery = true)
    BigInteger findStation(@Param("code") String code);


    @Query(value = "SELECT s.id FROM station as s WHERE s.code = stationCode", nativeQuery = true)
    BigInteger findCode(@Param("stationCode") String stationCode);

    @Modifying
    @Transactional
    @Query(value = "insert into station (code,head_station_sign_off_time,head_station_sign_on_time,name,no_of_beds,out_station_sign_off_time,out_station_sign_on_time) values(:stationCode,:headStationSignOffTime,:headStationSignOnTime,'',:noOfBeds,:outStationSignOnTime,:outStationSignOffTime)", nativeQuery = true)
    void saveStationData(
            @Param("stationCode") String stationCode,
            @Param("headStationSignOffTime") Long headStationSignOffTime,
            @Param("headStationSignOnTime") Long headStationSignOnTime,
            @Param("noOfBeds") Long noOfBeds,
            @Param("outStationSignOnTime") Long outStationSignOnTime,
            @Param("outStationSignOffTime") Long outStationSignOffTime

    );
}
