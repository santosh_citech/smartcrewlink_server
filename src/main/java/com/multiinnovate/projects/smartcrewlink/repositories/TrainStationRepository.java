package com.multiinnovate.projects.smartcrewlink.repositories;

import com.multiinnovate.projects.smartcrewlink.models.TrainStation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author : ADMIN 3/1/2022
 * @created : 01 / 03 / 2022 - 7:23 PM
 **/
@Repository
public interface TrainStationRepository extends JpaRepository<TrainStation, Long> {

    @Query(value = "SELECT ts.id FROM train_station as ts "
            + "WHERE ts.train_id = :trainId AND ts.stop_no = :stopNumber", nativeQuery = true)
    BigInteger findTrainStationId(@Param("trainId") BigInteger trainId, @Param("stopNumber") Integer stopNumber);

    @Query(value = "SELECT " +
            "ts.departure_time," +
            "ts.day," +
            "ts.arrival_time," +
            "ts.journey_duration " +
            "FROM train_station as ts "
            + "WHERE ts.train_id = :trainId AND ts.stop_no = :stopNumber", nativeQuery = true)
    List<Object[]> findByJourneyDurationAndDepartureTime(@Param("trainId") Long trainId, @Param("stopNumber") int stopNumber);


//    @Modifying
//    @Transactional
//    @Query(value = "INSERT INTO train_station (" +
//            "train_no," +
//            "stop_no," +
//            "station_code," +
//            "day_of_journey," +
//            "arrival_time," +
//            "departure_time," +
//            "distance," +
//            "loco_type," +
//            "arrival_in_minutes," +
//            "departure_in_minutes," +
//            "train_id," +
//            "station_id," +
//            "arrival_day," +
//            "departure_day," +
//            "arrival_date_time," +
//            "departure_date_time" +
//            ") values (" +
//            ":trainNo," +
//            ":stopNo," +
//            ":stationCode," +
//            ":dayOfJourney," +
//            ":arrivalTime," +
//            ":departureTime," +
//            ":distance," +
//            ":arrivalInMinutes," +
//            ":departureInMinutes," +
//            ":trainId," +
//            ":codeId," +
//            ":arrivalDay," +
//            ":departureDay," +
//            ":arrivalDateTime," +
//            ":departureDateTime" +
//            ")", nativeQuery = true)
//    void saveTrainStationData(
//            @Param("trainNo") Long trainNo,
//            @Param("stopNo") Long stopNo,
//            @Param("stationCode") String stationCode,
//            @Param("dayOfJourney") Long dayOfJourney,
//            @Param("arrivalTime") String arrivalTime,
//            @Param("departureTime") String departureTime,
//            @Param("distance") Long distance,
//            @Param("locoType") String locoType,
//            @Param("arrivalInMinutes") Long arrivalInMinutes,
//            @Param("departureInMinutes") Long departureInMinutes,
//            @Param("trainId") Long trainId,
//            @Param("codeId") Long codeId,
//            @Param("arrivalDay") Long arrivalDay,
//            @Param("departureDay") Long departureDay,
//            @Param("arrivalDateTime") LocalDateTime arrivalDateTime,
//            @Param("departureDateTime") LocalDateTime departureDateTime);

}
