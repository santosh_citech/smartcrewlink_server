package com.multiinnovate.projects.smartcrewlink.components;

/**
 * @author : ADMIN 3/2/2022
 * @created : 02 / 03 / 2022 - 2:20 AM
 **/

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Component
public class TimeCalculatorComponent {


    @PostConstruct
    public void runAfterObjectCreated() {
        System.out.println("PostContruct method called");
    }

    public void getTimeDiff() {

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            LocalDateTime dateTime1 = LocalDateTime.parse("2022-03-01 19:00:00", formatter);
            LocalDateTime dateTime2 = LocalDateTime.parse("2022-03-01 16:00:00", formatter);

            long diffInMilli = Math.abs(java.time.Duration.between(dateTime1, dateTime2).toMillis());
            long diffInSeconds = Math.abs(java.time.Duration.between(dateTime1, dateTime2).getSeconds());
            long diffInMinutes = Math.abs(java.time.Duration.between(dateTime1, dateTime2).toMinutes());
        } catch (Exception ex) {
            System.out.println("Error :" + ex);
        }

    }


    public void getTimeDifference() throws ParseException {
        String time01 = "07:22:00";
        String time02 = "07:10:00";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime dd1 = LocalTime.parse(time01, formatter);
        LocalTime dd2 = LocalTime.parse(time02, formatter);
        long diffInMilli = Math.abs(java.time.Duration.between(dd1, dd2).toMillis());
        long diffInSeconds = Math.abs(java.time.Duration.between(dd1, dd2).getSeconds());
        long diffInMinutes = Math.abs(java.time.Duration.between(dd1, dd2).toMinutes());

    }
}
